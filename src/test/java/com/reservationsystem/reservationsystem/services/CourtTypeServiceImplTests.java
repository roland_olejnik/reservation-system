package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.CourtTypeDao;
import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.services.impl.CourtTypeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

/**
 * Tests for CourtTypeServiceImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations="classpath:application.yaml", properties = "spring.sql.init.mode=never")
public class CourtTypeServiceImplTests {

    private final CourtTypeDao courtTypeDao;
    private final CourtTypeService courtTypeService;

    @Autowired
    public CourtTypeServiceImplTests(CourtTypeDao courtTypeDao) {
        this.courtTypeDao = courtTypeDao;
        this.courtTypeService = new CourtTypeServiceImpl(courtTypeDao);
    }


    @Test
    public void testCreateCourtType() throws CallInContextNotValidException {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate = courtTypeService.create(courtType);
        Assertions.assertEquals(resultCreate.getId(), 1L);
    }

    @Test
    public void testRetrieveAllCourtTypes() throws CallInContextNotValidException {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeService.create(courtType);

        List<CourtType> resultRetrieve = courtTypeService.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
    }

    @Test
    public void testRetrieveCourtType() throws CallInContextNotValidException {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeService.create(courtType);

        Optional<CourtType> resultRetrieve = courtTypeService.findOne(1L);
        Assertions.assertTrue(resultRetrieve.isPresent());
    }

    @Test
    public void testUpdateCourtType() throws CallInContextNotValidException {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeService.create(courtType);
        CourtType courtTypeAlt = TestDataUtil.createTestCourtTypeTwo();
        courtTypeAlt.setId(1L);

        CourtType resultRetrieve = courtTypeService.update(1L, courtTypeAlt);
        Assertions.assertEquals(resultRetrieve, courtTypeAlt);
    }

    @Test
    public void testDeleteCourtType() throws CallInContextNotValidException {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeService.create(courtType);

        courtTypeService.delete(1L);
        Assertions.assertTrue(courtTypeService.findAll().isEmpty());
    }
}
