package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.UserDao;
import com.reservationsystem.reservationsystem.domain.User;
import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;


/**
 * Tests for UserServiceImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations="classpath:application.yaml", properties = "spring.sql.init.mode=never")
public class UserServiceImplTests {

    private final UserDao userDao;
    private final UserService userService;

    @Autowired
    public UserServiceImplTests(UserDao userDao) {
        this.userDao = userDao;
        this.userService = new UserServiceImpl(userDao);
    }


    @Test
    public void testCreateUser() throws CallInContextNotValidException {
        User user = TestDataUtil.createTestUserOne();

        User resultCreate = userService.create(user);
        Assertions.assertEquals(resultCreate.getTelephoneNumber(), "+421123456");
    }

    @Test
    public void testRetrieveAllUsers() throws CallInContextNotValidException {
        User user = TestDataUtil.createTestUserOne();
        userService.create(user);

        List<User> resultRetrieve = userService.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
    }

    @Test
    public void testRetrieveUser() throws CallInContextNotValidException {
        User user = TestDataUtil.createTestUserOne();
        userService.create(user);

        Optional<User> resultRetrieve = userService.findOne("+421123456");
        Assertions.assertTrue(resultRetrieve.isPresent());
    }

    @Test
    public void testUpdateUser() throws CallInContextNotValidException {
        User user = TestDataUtil.createTestUserOne();
        userService.create(user);
        User userAlt = TestDataUtil.createTestUserTwo();
        userAlt.setTelephoneNumber("+421123456");

        User resultRetrieve = userService.update("+421123456", userAlt);
        Assertions.assertEquals(resultRetrieve, userAlt);
    }

    @Test
    public void testDeleteUser() throws CallInContextNotValidException {
        User user = TestDataUtil.createTestUserOne();
        userService.create(user);

        userService.delete("+421123456");
        Assertions.assertTrue(userService.findAll().isEmpty());
    }
}

