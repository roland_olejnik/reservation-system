package com.reservationsystem.reservationsystem;

import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.domain.User;
import com.reservationsystem.reservationsystem.domain.dto.CourtDto;
import com.reservationsystem.reservationsystem.domain.dto.CourtTypeDto;
import com.reservationsystem.reservationsystem.domain.dto.ReservationDto;
import com.reservationsystem.reservationsystem.domain.dto.UserDto;

import java.time.LocalDateTime;


/**
 * Data used in tests
 *
 * @author Roland Olejník
 */
public final class TestDataUtil {
    private TestDataUtil(){
    }

    public static CourtType createExistingTestCourtTypeOne() {
        return CourtType.builder()
                .id(-1L)
                .name("Acrylic")
                .price(125.0)
                .build();
    }

    public static CourtType createExistingTestCourtTypeTwo() {
        return CourtType.builder()
                .id(-2L)
                .name("Artificial grass")
                .price(200.0)
                .build();
    }

   public static CourtType createTestCourtTypeOne() {
        return CourtType.builder()
                .name("test_surface")
                .price(50.0)
                .build();
   }

    public static CourtType createTestCourtTypeTwo() {
        return CourtType.builder()
                .name("test_surface_two")
                .price(75.0)
                .build();
    }

   public static Court createTestCourtOne() {
        CourtType courtType = createTestCourtTypeOne();
        courtType.setId(1L);
       return Court.builder()
               .courtType(courtType)
               .build();
   }

    public static Court createTestCourtTwo() {
        CourtType courtType = createTestCourtTypeTwo();
        courtType.setId(2L);
        return Court.builder()
                .courtType(courtType)
                .build();
    }

    public static Court createExistingTestCourtOne() {
        CourtType courtType = createExistingTestCourtTypeOne();
        courtType.setId(-1L);
        return Court.builder()
                .courtType(courtType)
                .build();
    }

    public static Court createExistingTestCourtTwo() {
        CourtType courtType = createExistingTestCourtTypeTwo();
        courtType.setId(-2L);
        return Court.builder()
                .courtType(courtType)
                .build();
    }

   public static User createTestUserOne() {
        return User.builder()
                .telephoneNumber("+421123456")
                .name("John")
                .surname("Doe")
                .build();
   }

    public static User createTestUserTwo() {
        return User.builder()
                .telephoneNumber("+421987789")
                .name("Jane")
                .surname("Eod")
                .build();
    }

   public static Reservation createTestReservationOne() {
        Court court = createTestCourtOne();
        court.setId(1L);
        return Reservation.builder()
                .court(court)
                .user(createTestUserOne())
                .reservationFrom(LocalDateTime.of(2020, 3, 5,16,5))
                .reservationTo(LocalDateTime.of(2020, 3, 5,17,5))
                .doubles(false)
                .price(1000.0)
                .reservationCreationTimestamp(LocalDateTime.of(2020, 3, 4,17,5))
                .build();
   }

    public static Reservation createTestReservationTwo() {
        Court court = createTestCourtTwo();
        court.setId(2L);
        return Reservation.builder()
                .court(court)
                .user(createTestUserTwo())
                .reservationFrom(LocalDateTime.of(2030, 3, 5,16,5))
                .reservationTo(LocalDateTime.of(2030, 3, 5,17,5))
                .doubles(true)
                .price(1000.0)
                .reservationCreationTimestamp(LocalDateTime.of(2029, 3, 4,17,5))
                .build();
    }

    public static CourtTypeDto createExistingTestCourtTypeDtoOne() {
        return CourtTypeDto.builder()
                .id(-1L)
                .name("Acrylic")
                .price(125.0)
                .build();
    }

    public static CourtTypeDto createExistingTestCourtTypeDtoTwo() {
        return CourtTypeDto.builder()
                .id(-2L)
                .name("Artificial grass")
                .price(200.0)
                .build();
    }

    public static CourtTypeDto createTestCourtTypeDtoOne() {
        return CourtTypeDto.builder()
                .name("test_surface")
                .price(50.0)
                .build();
    }

    public static CourtTypeDto createTestCourtTypeDtoTwo() {
        return CourtTypeDto.builder()
                .name("test_surface_two")
                .price(75.0)
                .build();
    }

    public static CourtDto createTestCourtDtoOne() {
        CourtTypeDto courtTypeDto = createExistingTestCourtTypeDtoOne();
        return CourtDto.builder()
                .courtType(courtTypeDto)
                .build();
    }

    public static CourtDto createTestCourtDtoTwo() {
        CourtTypeDto courtTypeDto = createExistingTestCourtTypeDtoTwo();
        return CourtDto.builder()
                .courtType(courtTypeDto)
                .build();
    }

    public static CourtDto createExistingTestCourtDtoOne() {
        CourtTypeDto courtTypeDto = createExistingTestCourtTypeDtoOne();
        courtTypeDto.setId(-1L);
        return CourtDto.builder()
                .id(-1L)
                .courtType(courtTypeDto)
                .build();
    }

    public static CourtDto createExistingTestCourtDtoTwo() {
        CourtTypeDto courtTypeDto = createExistingTestCourtTypeDtoTwo();
        courtTypeDto.setId(-2L);
        return CourtDto.builder()
                .id(-2L)
                .courtType(courtTypeDto)
                .build();
    }

    public static UserDto createTestUserDtoOne() {
        return UserDto.builder()
                .telephoneNumber("+421123456")
                .name("John")
                .surname("Doe")
                .build();
    }

    public static UserDto createTestUserDtoTwo() {
        return UserDto.builder()
                .telephoneNumber("+421987789")
                .name("Jane")
                .surname("Eod")
                .build();
    }

    public static ReservationDto createTestReservationDtoOne() {
        CourtDto courtDto = createExistingTestCourtDtoOne();
        return ReservationDto.builder()
                .court(courtDto)
                .user(createTestUserDtoOne())
                .reservationFrom(LocalDateTime.of(2020, 3, 5,16,5,1))
                .reservationTo(LocalDateTime.of(2020, 3, 5,17,5,1))
                .doubles(false)
                .price(1000.0)
                .reservationCreationTimestamp(LocalDateTime.of(2020, 3, 4,17,5,1))
                .build();
    }

    public static ReservationDto createTestReservationDtoTwo() {
        CourtDto courtDto = createExistingTestCourtDtoTwo();
        return ReservationDto.builder()
                .court(courtDto)
                .user(createTestUserDtoTwo())
                .reservationFrom(LocalDateTime.of(2030, 3, 5,16,5,1))
                .reservationTo(LocalDateTime.of(2030, 3, 5,17,5,1))
                .doubles(true)
                .price(1000.0)
                .reservationCreationTimestamp(LocalDateTime.of(2029, 3, 4,17,5,1))
                .build();
    }

}
