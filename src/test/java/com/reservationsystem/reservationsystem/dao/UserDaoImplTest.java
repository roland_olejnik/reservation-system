package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.impl.UserDaoImpl;
import com.reservationsystem.reservationsystem.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

/**
 * Tests for UserDaoImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserDaoImplTest {

    private final UserDaoImpl userDao;

    @Autowired
    public UserDaoImplTest(UserDaoImpl userDao) {
        this.userDao = userDao;
    }


    @Test
    public void testCreateUserInDatabase(){
        User user = TestDataUtil.createTestUserOne();

        User resultCreate = userDao.create(user);
        Assertions.assertEquals(resultCreate.getTelephoneNumber(), "+421123456");
        Assertions.assertEquals(resultCreate, user);
    }

    @Test
    public void testUpdateUserInDatabase(){
        User user = TestDataUtil.createTestUserOne();
        User userAlt = TestDataUtil.createTestUserTwo();

        userDao.create(user);
        User resultUpdate = userDao.update(userAlt);
        Assertions.assertEquals(resultUpdate, userAlt);
    }

    @Test
    public void testRetrieveUserFromDatabase() {
        User user = TestDataUtil.createTestUserOne();

        User resultCreate =  userDao.create(user);
        User resultRetrieve = userDao.findOne("+421123456");
        Assertions.assertEquals(resultCreate, resultRetrieve);
    }

    @Test
    public void testRetrieveAllUsersFromDatabase() {
        User user = TestDataUtil.createTestUserOne();

        User resultCreate =  userDao.create(user);
        List<User> resultRetrieve = userDao.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
        Assertions.assertEquals(resultCreate, resultRetrieve.get(0));
    }

    @Test
    public void testDeleteUserFromDatabase() {
        User user = TestDataUtil.createTestUserOne();

        User resultCreate =  userDao.create(user);
        userDao.delete(resultCreate);
        List<User> resultRetrieve = userDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }

    @Test
    public void testDeleteByIdUserFromDatabase() {
        User user = TestDataUtil.createTestUserOne();

        User resultCreate =  userDao.create(user);
        userDao.deleteById(resultCreate.getTelephoneNumber());
        List<User> resultRetrieve = userDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }
}
