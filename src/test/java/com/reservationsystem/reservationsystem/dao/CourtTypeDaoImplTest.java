package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.impl.CourtTypeDaoImpl;
import com.reservationsystem.reservationsystem.domain.CourtType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

/**
 * Tests for CourtTypeDaoImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations="classpath:application.yaml", properties = "spring.sql.init.mode=never")
public class CourtTypeDaoImplTest {

    private final CourtTypeDaoImpl courtTypeDao;

    @Autowired
    public CourtTypeDaoImplTest(CourtTypeDaoImpl courtTypeDao) {
        this.courtTypeDao = courtTypeDao;
    }


    @Test
    public void testCreateCourtTypeInDatabase(){
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate = courtTypeDao.create(courtType);
        Assertions.assertEquals(resultCreate.getId(), 1L);
    }

    @Test
    public void testUpdateCourtTypeInDatabase(){
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        CourtType courtTypeAlt = TestDataUtil.createTestCourtTypeTwo();
        courtTypeAlt.setId(1L);

        courtTypeDao.create(courtType);
        CourtType resultUpdate = courtTypeDao.update(courtTypeAlt);
        Assertions.assertEquals(resultUpdate, courtTypeAlt);
    }

    @Test
    public void testRetrieveCourtTypeFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate =  courtTypeDao.create(courtType);
        CourtType resultRetrieve = courtTypeDao.findOne(1L);
        Assertions.assertEquals(resultCreate, resultRetrieve);
    }

    @Test
    public void testRetrieveAllCourtTypesFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate =  courtTypeDao.create(courtType);
        List<CourtType> resultRetrieve = courtTypeDao.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
        Assertions.assertEquals(resultCreate, resultRetrieve.get(0));
    }

    @Test
    public void testDeleteCourtTypeFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate =  courtTypeDao.create(courtType);
        courtTypeDao.delete(resultCreate);
        List<CourtType> resultRetrieve = courtTypeDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }

    @Test
    public void testDeleteByIdCourtTypeFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();

        CourtType resultCreate =  courtTypeDao.create(courtType);
        courtTypeDao.deleteById(resultCreate.getId());
        List<CourtType> resultRetrieve = courtTypeDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }
}
