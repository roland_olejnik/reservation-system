package com.reservationsystem.reservationsystem.dao;


import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.impl.CourtDaoImpl;
import com.reservationsystem.reservationsystem.dao.impl.CourtTypeDaoImpl;
import com.reservationsystem.reservationsystem.dao.impl.ReservationDaoImpl;
import com.reservationsystem.reservationsystem.dao.impl.UserDaoImpl;
import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * Tests for ReservationDaoImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ReservationDaoImplTest {

    private final CourtTypeDaoImpl courtTypeDao;
    private final CourtDaoImpl courtDao;
    private final ReservationDaoImpl reservationDao;
    private final UserDaoImpl userDao;

    @Autowired
    public ReservationDaoImplTest(CourtTypeDaoImpl courtTypeDao, CourtDaoImpl courtDao,
                                  ReservationDaoImpl reservationDao, UserDaoImpl userDao) {
        this.courtTypeDao = courtTypeDao;
        this.courtDao = courtDao;
        this.reservationDao = reservationDao;
        this.userDao = userDao;
    }

    @Test
    public void testCreateReservationInDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        Reservation resultCreate = reservationDao.create(reservation);
        Assertions.assertEquals(resultCreate.getId(), 1L);
    }

    @Test
    public void testUpdateReservationInDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        User userAlt = TestDataUtil.createTestUserTwo();
        userDao.create(userAlt);
        CourtType courtTypeAlt = TestDataUtil.createTestCourtTypeTwo();
        courtTypeDao.create(courtTypeAlt);
        Court courtAlt = TestDataUtil.createTestCourtTwo();
        courtDao.create(courtAlt);
        Reservation reservationAlt = TestDataUtil.createTestReservationTwo();
        reservationAlt.setId(1L);

        reservationDao.create(reservation);
        Reservation resultUpdate = reservationDao.update(reservationAlt);
        Assertions.assertEquals(resultUpdate, reservationAlt);
    }

    @Test
    public void testRetrieveReservationFromDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        Reservation resultCreate =  reservationDao.create(reservation);
        Reservation resultRetrieve = reservationDao.findOne(1L);
        Assertions.assertEquals(resultCreate, resultRetrieve);
    }

    @Test
    public void testRetrieveAllReservationsFromDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        Reservation resultCreate =  reservationDao.create(reservation);
        List<Reservation> resultRetrieve = reservationDao.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
        Assertions.assertEquals(resultCreate, resultRetrieve.get(0));
    }

    @Test
    public void testDeleteReservationFromDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        Reservation resultCreate =  reservationDao.create(reservation);
        reservationDao.delete(resultCreate);
        List<Reservation> resultRetrieve = reservationDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }

    @Test
    public void testDeleteByIdReservationFromDatabase(){
        User user = TestDataUtil.createTestUserOne();
        userDao.create(user);
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();
        courtDao.create(court);
        Reservation reservation = TestDataUtil.createTestReservationOne();

        Reservation resultCreate =  reservationDao.create(reservation);
        reservationDao.deleteById(resultCreate.getId());
        List<Reservation> resultRetrieve = reservationDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }

}
