package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.dao.impl.CourtDaoImpl;
import com.reservationsystem.reservationsystem.dao.impl.CourtTypeDaoImpl;
import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.CourtType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * Tests for CourtDaoImpl
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations="classpath:application.yaml", properties = "spring.sql.init.mode=never")
public class CourtDaoImplTest {

    private final CourtTypeDaoImpl courtTypeDao;
    private final CourtDaoImpl courtDao;

    @Autowired
    public CourtDaoImplTest(CourtTypeDaoImpl courtTypeDao, CourtDaoImpl courtDao) {
        this.courtTypeDao = courtTypeDao;
        this.courtDao = courtDao;
    }

    @Test
    public void testCreateCourtInDatabase(){
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        Court resultCreate = courtDao.create(court);
        Assertions.assertEquals(resultCreate.getId(), 1L);
    }

    @Test
    public void testUpdateCourtInDatabase(){
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        CourtType courtTypeAlt = TestDataUtil.createTestCourtTypeTwo();
        courtTypeDao.create(courtTypeAlt);
        Court courtAlt = TestDataUtil.createTestCourtTwo();
        courtAlt.setId(1L);

        courtDao.create(court);
        Court resultUpdate = courtDao.update(courtAlt);
        Assertions.assertEquals(resultUpdate, courtAlt);
    }

    @Test
    public void testRetrieveCourtFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        Court resultCreate =  courtDao.create(court);
        Court resultRetrieve = courtDao.findOne(1L);
        Assertions.assertEquals(resultCreate, resultRetrieve);
    }

    @Test
    public void testRetrieveAllCourtsFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        Court resultCreate =  courtDao.create(court);
        List<Court> resultRetrieve = courtDao.findAll();
        Assertions.assertEquals(resultRetrieve.size(), 1);
        Assertions.assertEquals(resultCreate, resultRetrieve.get(0));
    }

    @Test
    public void testDeleteCourtFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        Court resultCreate =  courtDao.create(court);
        courtDao.delete(resultCreate);
        List<Court> resultRetrieve = courtDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }

    @Test
    public void testDeleteByIdCourtFromDatabase() {
        CourtType courtType = TestDataUtil.createTestCourtTypeOne();
        courtTypeDao.create(courtType);
        Court court = TestDataUtil.createTestCourtOne();

        Court resultCreate =  courtDao.create(court);
        courtDao.deleteById(resultCreate.getId());
        List<Court> resultRetrieve = courtDao.findAll();
        Assertions.assertTrue(resultRetrieve.isEmpty());
    }
}
