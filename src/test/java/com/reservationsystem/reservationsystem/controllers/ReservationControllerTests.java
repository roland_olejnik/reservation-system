package com.reservationsystem.reservationsystem.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.domain.dto.ReservationDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


/**
 * Tests for ReservationController
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class ReservationControllerTests {

    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    @Autowired
    public ReservationControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
        this.objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void testCreateReservationReturnsHttpCode201() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        ).andExpect(
                MockMvcResultMatchers.status().isCreated()
        );
    }

    @Test
    public void testCreateInvalidReservationEndPrecedesStartReturnsHttpCode404() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        reservation.setReservationTo(LocalDateTime.now().minusDays(5));
        reservation.setReservationFrom(LocalDateTime.now().plusDays(5));
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        ).andExpect(
                MockMvcResultMatchers.status().isBadRequest()
        );
    }

    @Test
    public void testCreateInvalidReservationTimeCollisionReturnsHttpCode404() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        reservation.setReservationTo(LocalDateTime.now().minusDays(5));
        reservation.setReservationFrom(LocalDateTime.now().plusDays(5));

        ReservationDto reservationAlt = TestDataUtil.createTestReservationDtoTwo();
        reservationAlt.setReservationTo(LocalDateTime.now().minusDays(4));
        reservationAlt.setReservationFrom(LocalDateTime.now().plusDays(4));

        String reservationJson = objectMapper.writeValueAsString(reservation);
        String reservationJsonAlt = objectMapper.writeValueAsString(reservationAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJsonAlt)
        ).andExpect(
                MockMvcResultMatchers.status().isBadRequest()
        );

    }

    @Test
    public void testCreateReservationReturnsCreatedReservation() throws Exception {
        ReservationDto reservationDto = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservationDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.court.id").value(reservationDto.getCourt().getId())
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.user.telephoneNumber").value(reservationDto.getUser()
                        .getTelephoneNumber())
        );
    }

    @Test
    public void testReturnAllReservationsReturnsHttpCode200() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testReturnAllReservationsReturnsList() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].court.id").value(reservation.getCourt().getId())
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].user.telephoneNumber").value(reservation.getUser()
                        .getTelephoneNumber())
        );
    }

    @Test
    public void testReturnReservationReturnsHttpCode200() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/1")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testReturnNonExistingReservationReturnsHttpCode404() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/999")
        ).andExpect(
                MockMvcResultMatchers.status().isNotFound()
        );
    }

    @Test
    public void testReturnReservationReturnsReservation() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/1")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.court.id").value(reservation.getCourt().getId())
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.user.telephoneNumber").value(reservation.getUser()
                        .getTelephoneNumber())
        );
    }

    @Test
    public void testReturnReservationsByCourtReturnsHttpCode200() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/court/1")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testReturnReservationsByCourtReturnsReservation() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/court/-1")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].court.id").value(reservation.getCourt().getId())
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].user.telephoneNumber").value(reservation.getUser()
                        .getTelephoneNumber())
        );
    }

    @Test
    public void testReturnReservationsByUserReturnsHttpCode200() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/user/'+421123456'/false")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testReturnReservationsByUserReturnsReservation() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/user/+421123456/false")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].court.id").value(reservation.getCourt().getId())
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[0].user.telephoneNumber").value(reservation.getUser()
                        .getTelephoneNumber())
        );
    }

    @Test
    public void testReturnReservationsByUserReturnsReservationOnlyFuture() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/reservations/user/+421123456/True")
        ).andExpect(content().string("[]"));
    }


    @Test
    public void testUpdateReturnsHttpCode200() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        ReservationDto reservationDtoAlt = TestDataUtil.createTestReservationDtoTwo();
        reservationDtoAlt.setId(1L);
        String reservationJsonAlt = objectMapper.writeValueAsString(reservationDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/reservations/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJsonAlt)
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testUpdateNonExistingReservationReturnsHttpCode404() throws Exception {
        ReservationDto reservationDtoAlt = TestDataUtil.createTestReservationDtoTwo();
        reservationDtoAlt.setId(1L);
        String reservationJsonAlt = objectMapper.writeValueAsString(reservationDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/reservations/-999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJsonAlt)
        ).andExpect(
                MockMvcResultMatchers.status().isNotFound()
        );
    }

    @Test
    public void testUpdateReturnsUpdatedReservation() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        ReservationDto reservationDtoAlt = TestDataUtil.createTestReservationDtoTwo();
        reservationDtoAlt.setId(1L);
        String reservationJsonAlt = objectMapper.writeValueAsString(reservationDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/reservations/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJsonAlt)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.court.id").value(reservationDtoAlt.getCourt().getId())
        );
    }

    @Test
    public void testDeleteExistingReservationReturnsHttpCode204() throws Exception {
        ReservationDto reservation = TestDataUtil.createTestReservationDtoOne();
        String reservationJson = objectMapper.writeValueAsString(reservation);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/reservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reservationJson)
        );

        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/reservations/1")
        ).andExpect(
                MockMvcResultMatchers.status().isNoContent()
        );
    }

    @Test
    public void testDeleteNonExistingReservationReturnsHttpCode500() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/reservations/-999")
        ).andExpect(
                MockMvcResultMatchers.status().isInternalServerError()
        );
    }
}
