package com.reservationsystem.reservationsystem.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reservationsystem.reservationsystem.TestDataUtil;
import com.reservationsystem.reservationsystem.domain.dto.CourtDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Tests for CourtController
 *
 * @author Roland Olejník
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class CourtControllerTests {

    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    @Autowired
    public CourtControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void testCreateCourtReturnsHttpCode201() throws Exception {
        CourtDto court = TestDataUtil.createTestCourtDtoOne();
        String courtJson = objectMapper.writeValueAsString(court);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/courts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(courtJson)
        ).andExpect(
                MockMvcResultMatchers.status().isCreated()
        );
    }

    @Test
    public void testCreateCourtReturnsCreatedCourt() throws Exception {
        CourtDto courtDto = TestDataUtil.createTestCourtDtoOne();
        String courtJson = objectMapper.writeValueAsString(courtDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/courts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(courtJson)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.courtType.id").value(courtDto.getCourtType().getId())
        );
    }

    @Test
    public void testReturnAllCourtsReturnsHttpCode200() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/courts")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testCreateCourtReturnsList() throws Exception {
        CourtDto courtDto = TestDataUtil.createExistingTestCourtDtoOne();
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/courts")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[3].id").value(-1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$[3].courtType.id").value(courtDto.getCourtType().getId())
        );
    }

    @Test
    public void testReturnCourtReturnsHttpCode200() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/courts/-1")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testReturnNonExistingCourtReturnsHttpCode404() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/courts/-999")
        ).andExpect(
                MockMvcResultMatchers.status().isNotFound()
        );
    }

    @Test
    public void testReturnCourtReturnsBook() throws Exception {
        CourtDto courtDto = TestDataUtil.createExistingTestCourtDtoOne();
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/courts/-1")
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(-1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.courtType.id").value(courtDto.getCourtType().getId())
        );
    }

    @Test
    public void testUpdateReturnsHttpCode200() throws Exception {
        CourtDto courtDtoAlt = TestDataUtil.createExistingTestCourtDtoTwo();
        String courtJson = objectMapper.writeValueAsString(courtDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/courts/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(courtJson)
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }

    @Test
    public void testUpdateNonExistingCourtReturnsHttpCode404() throws Exception {
        CourtDto courtDtoAlt = TestDataUtil.createExistingTestCourtDtoTwo();
        String courtJson = objectMapper.writeValueAsString(courtDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/courts/-999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(courtJson)
        ).andExpect(
                MockMvcResultMatchers.status().isNotFound()
        );
    }

    @Test
    public void testUpdateReturnsUpdatedCourt() throws Exception {
        CourtDto courtDtoAlt = TestDataUtil.createExistingTestCourtDtoTwo();
        String courtJson = objectMapper.writeValueAsString(courtDtoAlt);
        mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/courts/-1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(courtJson)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id").value(-1)
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.courtType.id").value(courtDtoAlt.getCourtType().getId())
        );
    }

    @Test
    public void testDeleteExistingCourtReturnsHttpCode204() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/courts/-1")
        ).andExpect(
                MockMvcResultMatchers.status().isNoContent()
        );
    }

    @Test
    public void testDeleteNonExistingCourtReturnsHttpCode500() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/courts/-999")
        ).andExpect(
                MockMvcResultMatchers.status().isInternalServerError()
        );
    }

}
