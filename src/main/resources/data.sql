INSERT INTO court_types (id, name, price, removed) VALUES
(-1, 'Acrylic', 125, False),
(-2, 'Artificial grass', 200, False);

INSERT INTO courts (id, court_type_id, removed) VALUES
(-1, -1, False),
(-2, -2, False),
(-3, -1, False),
(-4, -2, False);