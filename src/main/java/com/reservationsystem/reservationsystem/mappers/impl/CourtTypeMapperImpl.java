package com.reservationsystem.reservationsystem.mappers.impl;

import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.domain.dto.CourtTypeDto;
import com.reservationsystem.reservationsystem.mappers.Mapper;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Class that handles mapping of CourType entity to CourtTypeDto or in reverse
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
@Component
public class CourtTypeMapperImpl implements Mapper<CourtType, CourtTypeDto> {

    private ModelMapper modelMapper;

    @Override
    public CourtTypeDto mapTo(CourtType courtType) {
        return modelMapper.map(courtType, CourtTypeDto.class);
    }

    @Override
    public CourtType mapFrom(CourtTypeDto courtTypeDto) {
        return modelMapper.map(courtTypeDto, CourtType.class);
    }
}
