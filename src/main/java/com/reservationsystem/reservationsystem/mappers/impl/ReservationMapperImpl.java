package com.reservationsystem.reservationsystem.mappers.impl;

import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.domain.dto.ReservationDto;
import com.reservationsystem.reservationsystem.mappers.Mapper;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Class that handles mapping of Reservation entity to ReservationDto or in reverse
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
@Component
public class ReservationMapperImpl implements Mapper<Reservation, ReservationDto> {

    private ModelMapper modelMapper;

    @Override
    public ReservationDto mapTo(Reservation reservation) {
        return modelMapper.map(reservation, ReservationDto.class);
    }

    @Override
    public Reservation mapFrom(ReservationDto reservationDto) {
        return modelMapper.map(reservationDto, Reservation.class);
    }
}
