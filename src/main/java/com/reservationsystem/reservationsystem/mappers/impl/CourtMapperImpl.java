package com.reservationsystem.reservationsystem.mappers.impl;

import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.dto.CourtDto;
import com.reservationsystem.reservationsystem.mappers.Mapper;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Class that handles mapping of Court entity to CourtDto or in reverse
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
@Component
public class CourtMapperImpl implements Mapper<Court, CourtDto> {

    private ModelMapper modelMapper;

    @Override
    public CourtDto mapTo(Court court) {
        return modelMapper.map(court, CourtDto.class);
    }

    @Override
    public Court mapFrom(CourtDto courtDto) {
        return modelMapper.map(courtDto, Court.class);
    }
}
