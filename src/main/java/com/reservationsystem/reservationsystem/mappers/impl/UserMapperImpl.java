package com.reservationsystem.reservationsystem.mappers.impl;

import com.reservationsystem.reservationsystem.domain.User;
import com.reservationsystem.reservationsystem.domain.dto.UserDto;
import com.reservationsystem.reservationsystem.mappers.Mapper;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Class that handles mapping of User entity to CourtDto or in reverse
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
@Component
public class UserMapperImpl implements Mapper<User, UserDto> {

    private ModelMapper modelMapper;

    @Override
    public UserDto mapTo(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public User mapFrom(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }
}
