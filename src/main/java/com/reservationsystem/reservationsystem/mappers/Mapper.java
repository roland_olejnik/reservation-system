package com.reservationsystem.reservationsystem.mappers;

/**
 * Interface that declares methods used by the concrete mappers
 *
 * @author Roland Olejník
 */
public interface Mapper<A,B> {

    B mapTo(A a);

    A mapFrom(B b);
}
