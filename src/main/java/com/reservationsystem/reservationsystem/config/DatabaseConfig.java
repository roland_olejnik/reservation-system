package com.reservationsystem.reservationsystem.config;

import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.domain.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Bean;


/**
 *  Configuration class containing beans responsible for database configuration
 *
 * @author Roland Olejník
 */
@org.springframework.context.annotation.Configuration
public class DatabaseConfig {

    /**
     * Bean for creating SessionFactory from Hibernate configuration file
     *
     * @return created SessionFactory
     */
    @Bean
    public SessionFactory sessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(Court.class);
        configuration.addAnnotatedClass(CourtType.class);
        configuration.addAnnotatedClass(Reservation.class);
        configuration.addAnnotatedClass(User.class);

        return configuration.buildSessionFactory();
    }
}
