package com.reservationsystem.reservationsystem.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class containing beans for mappers
 *
 * @author Roland Olejník
 */
@Configuration
public class MapperConfig {

    /**
     * Bean responsible for creating and configuring ModelMapper
     *
     * @return configured ModelMapper
     */
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper;
    }
}
