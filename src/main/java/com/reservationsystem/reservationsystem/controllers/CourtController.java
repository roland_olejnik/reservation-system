package com.reservationsystem.reservationsystem.controllers;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.dto.CourtDto;

import com.reservationsystem.reservationsystem.mappers.Mapper;
import com.reservationsystem.reservationsystem.services.CourtService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller for court requests
 *
 * @author Roland Olejník
 */
@RestController
@AllArgsConstructor
public class CourtController {

    private CourtService courtService;

    private Mapper<Court, CourtDto> courtMapper;

    /**
     * Method responsible for handling create requests of courts
     *
     * @param courtDto CourtDto object extracted from requests body
     * @return Http status of 201 with DTO of created entity if successful or http status of 400/500 if not
     */
    @PostMapping(path = "/api/courts")
    public ResponseEntity<CourtDto> createCourt(@RequestBody CourtDto courtDto) {
        try {
            Court court = courtMapper.mapFrom(courtDto);
            Court savedCourt = courtService.create(court);
            return new ResponseEntity<>(courtMapper.mapTo(savedCourt), HttpStatus.CREATED);
        }
        catch (CallInContextNotValidException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method for handling requests to retrieve all courts from database
     *
     * @return Http status of 200 with list of all courts if successful or http status of 500 if request could not
     * be handled
     */
    @GetMapping(path = "/api/courts")
    public ResponseEntity<List<CourtDto>> retrieveAllCourts() {
        try {
            List<Court> courtEntities = courtService.findAll();
            return new ResponseEntity<>(courtEntities.stream().map(courtMapper::mapTo).collect(Collectors.toList()),
                    HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling requests to retrieve court with certain id
     *
     * @param id of specific court extracted from path
     * @return Http status of 200 with retrieved court if successful, http status of 404 if court was not found or
     * http status of 500 if request could not be handled
     */
    @GetMapping(path = "/api/courts/{id}")
    public ResponseEntity<CourtDto> retrieveOneCourt(@PathVariable("id") Long id) {
        try {
            Optional<Court> courtFound = courtService.findOne(id);
            return courtFound.map(court -> {
                CourtDto courtDto = courtMapper.mapTo(court);
                return new ResponseEntity<>(courtDto, HttpStatus.OK);
            }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method handling requests for updating courts
     *
     * @param id of specific court to be updated
     * @param courtDto CourtDto object with all attributes that are requested to be updated set to non-null value
     * @return Http status of 200 with updated court if successful, http status of 404 if court was not found or
     * http status of 500 if request could not be handled
     */
    @PatchMapping(path = "/api/courts/{id}")
    public ResponseEntity<CourtDto> updateCourt(@PathVariable("id") Long id,
                                              @RequestBody CourtDto courtDto) {
        try {
            if (courtService.findOne(id).isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            Court court = courtMapper.mapFrom(courtDto);
            Court courtResult = courtService.update(id, court);
            return new ResponseEntity<>(courtMapper.mapTo(courtResult), HttpStatus.OK);
        }
        catch (CallInContextNotValidException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling deletion requests of a court
     *
     * @param id of court to be deleted
     * @return Http status of 204 if delete was successful or https status of 500 if request could not be handled
     */
    @DeleteMapping(path = "/api/courts/{id}")
    public ResponseEntity<CourtDto> deleteCourt(@PathVariable("id") Long id) {
        try {
            courtService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
