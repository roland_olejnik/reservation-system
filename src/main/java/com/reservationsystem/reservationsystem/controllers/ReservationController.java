package com.reservationsystem.reservationsystem.controllers;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.domain.dto.ReservationDto;


import com.reservationsystem.reservationsystem.mappers.Mapper;
import com.reservationsystem.reservationsystem.services.ReservationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller for reservation requests
 *
 * @author Roland Olejník
 */
@RestController
@AllArgsConstructor
public class ReservationController {

    private ReservationService reservationService;

    private Mapper<Reservation, ReservationDto> reservationMapper;

    /**
     * Method handling create requests for reservations
     *
     * @param reservationDto DTO of reservation extracted from requests body
     * @return Http status of 201 with DTO of created entity if successful or http status of 500 if not
     */
    @PostMapping(path = "/api/reservations")
    public ResponseEntity<ReservationDto> createReservation(@RequestBody ReservationDto reservationDto) {
        try {
            Reservation reservation = reservationMapper.mapFrom(reservationDto);
            Reservation savedReservation = reservationService.create(reservation);
            return new ResponseEntity<>(reservationMapper.mapTo(savedReservation), HttpStatus.CREATED);
        }
        catch (CallInContextNotValidException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method for handling requests to retrieve all reservations from database
     *
     * @return Http status of 200 with list of all reservations if successful or http status of 500 if request could
     * not be handled
     */
    @GetMapping(path = "/api/reservations")
    public ResponseEntity<List<ReservationDto>> retrieveAllReservations() {
        try {
            List<Reservation> reservationEntities = reservationService.findAll();
            return new ResponseEntity<>(reservationEntities.stream().map(reservationMapper::mapTo)
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling requests to retrieve reservation with certain id
     *
     * @param id of specific reservation extracted from path
     * @return Http status of 200 with retrieved reservation if successful, http status of 404 if reservation
     * was not found or http status of 500 if request could not be handled
     */
    @GetMapping(path = "/api/reservations/{id}")
    public ResponseEntity<ReservationDto> retrieveOneReservation(@PathVariable("id") Long id) {
        try {
            Optional<Reservation> reservationFound = reservationService.findOne(id);
            return reservationFound.map(reservation -> {
                ReservationDto reservationDto = reservationMapper.mapTo(reservation);
                return new ResponseEntity<ReservationDto>(reservationDto, HttpStatus.OK);
            }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling requests to retrieve reservations by id of a court
     *
     * @param id of specific court extracted from path
     * @return Http status of 200 with retrieved reservations if successful or http status of 500
     * if request could not be handled
     */
    @GetMapping(path = "/api/reservations/court/{id}")
    public ResponseEntity<List<ReservationDto>> retrieveReservationsForCourt(@PathVariable("id") Long id) {
        try {
            List<Reservation> reservationEntities = reservationService.findReservationsByCourt(id);
            return new ResponseEntity<>(reservationEntities.stream().map(reservationMapper::mapTo)
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling requests to retrieve reservations of a certain user with a choice to retrieve
     * only future reservations
     *
     * @param telephoneNumber of a user extracted from the path
     * @param onlyFuture boolean setting if only future reservations are to be retrieved
     * @return Http status of 200 with retrieved reservations if successful or http status of 500
     * if request could not be handled
     */
    @GetMapping(path = "/api/reservations/user/{id}/{onlyFuture}")
    public ResponseEntity<List<ReservationDto>> retrieveReservationsForCourt(@PathVariable("id") String telephoneNumber,
                                                                             @PathVariable("onlyFuture") Boolean onlyFuture) {
        try {
            List<Reservation> reservationEntities = reservationService.findReservationsByUser(telephoneNumber, onlyFuture);
            return new ResponseEntity<>(reservationEntities.stream().map(reservationMapper::mapTo)
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method handling requests for updating reservations
     *
     * @param id of specific reservation to be updated
     * @param reservationDto DTO object with all attributes that are requested to be updated set to non-null value
     * @return Http status of 200 with updated reservation if successful, http status of 404 if reservation was not found or
     * http status of 500 if request could not be handled
     */
    @PatchMapping(path = "/api/reservations/{id}")
    public ResponseEntity<ReservationDto> updateReservation(@PathVariable("id") Long id,
                                                @RequestBody ReservationDto reservationDto) {
        try {
            if (reservationService.findOne(id).isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            Reservation reservation = reservationMapper.mapFrom(reservationDto);
            Reservation reservationResult = reservationService.update(id, reservation);
            return new ResponseEntity<ReservationDto>(reservationMapper.mapTo(reservationResult), HttpStatus.OK);
        }
        catch (CallInContextNotValidException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method responsible for handling deletion requests of a reservation
     *
     * @param id of reservation to be deleted
     * @return Http status of 204 if delete was successful or https status of 500 if request could not be handled
     */
    @DeleteMapping(path = "/api/reservations/{id}")
    public ResponseEntity<ReservationDto> deleteReservation(@PathVariable("id") Long id) {
        try {
            reservationService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}