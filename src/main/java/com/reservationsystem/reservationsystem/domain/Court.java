package com.reservationsystem.reservationsystem.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SoftDelete;

/**
 * Entity class representing court
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@SoftDelete(columnName = "removed")
@Table(name = "courts")
public class Court {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "court_type_id", nullable = false)
    private CourtType courtType;
}
