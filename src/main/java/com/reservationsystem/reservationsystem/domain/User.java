package com.reservationsystem.reservationsystem.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SoftDelete;

/**
 * Entity class representing user
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@SoftDelete(columnName = "removed")
@Table(name = "users")
public class User {

    @Id
    private String telephoneNumber;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;
}
