package com.reservationsystem.reservationsystem.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SoftDelete;

/**
 * Entity class representing court type
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@SoftDelete(columnName = "removed")
@Table(name = "court_types")
public class CourtType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double price;
}
