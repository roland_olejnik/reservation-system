package com.reservationsystem.reservationsystem.domain.dto;

import com.reservationsystem.reservationsystem.domain.CourtType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * DTO class of Court
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourtDto {

    private Long id;

    private CourtTypeDto courtType;
}
