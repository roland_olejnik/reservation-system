package com.reservationsystem.reservationsystem.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO class of CourtType
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourtTypeDto {

    private Long id;

    private String name;

    private Double price;
}