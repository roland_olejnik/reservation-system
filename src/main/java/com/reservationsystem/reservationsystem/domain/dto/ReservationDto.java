package com.reservationsystem.reservationsystem.domain.dto;

import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalDateTime;

/**
 * DTO class of Reservation
 *
 * @author Roland Olejník
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReservationDto {

    private Long id;

    private CourtDto court;

    private UserDto user;

    private LocalDateTime reservationFrom;

    private LocalDateTime reservationTo;

    private Boolean doubles;

    private double price;

    private LocalDateTime reservationCreationTimestamp;
}
