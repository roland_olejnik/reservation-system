package com.reservationsystem.reservationsystem.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SoftDelete;

import java.time.LocalDateTime;

/**
 * Entity class representing reservation
 *
 * @author Roland Olejník
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@SoftDelete(columnName = "removed")
@Table(name = "court_reservations")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "court_id", nullable = false)
    private Court court;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(nullable = false)
    private LocalDateTime reservationFrom;

    @Column(nullable = false)
    private LocalDateTime reservationTo;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Boolean doubles;

    @Column(nullable = false)
    private LocalDateTime reservationCreationTimestamp;
}
