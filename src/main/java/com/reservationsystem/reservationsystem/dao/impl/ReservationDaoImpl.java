package com.reservationsystem.reservationsystem.dao.impl;

import com.reservationsystem.reservationsystem.dao.ReservationDao;
import com.reservationsystem.reservationsystem.domain.Reservation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * DAO class of Reservation
 *
 * @author Roland Olejník
 */
@Component
public class ReservationDaoImpl extends AbstractGenericDaoImpl<Reservation, Long> implements ReservationDao {
    public ReservationDaoImpl(SessionFactory sessionFactory) {
        super(Reservation.class, sessionFactory);
    }

    /**
     * Method retrieving reservations on certain court from the table
     *
     * @param courtId id of a court that reservation has to contain to be retrieved
     * @return list of reservations on certain court
     */
    public List<Reservation> getReservationsByCourtId(long courtId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Reservation> result = session.createQuery
                ("FROM " + clazz.getName() + " R WHERE court.id = :courtId ORDER BY R.reservationCreationTimestamp DESC",
                        clazz)
                .setParameter("courtId", courtId)
                .getResultList();
        session.close();
        return result;
    }

    /**
     * Retrieves reservations from a certain user with an option to only retrieve future reservations
     *
     * @param telephoneNumber of a user whose reservations are to be retrieved
     * @param onlyFutureReservations boolean if only future reservation are to be retrieved
     * @return list of reservations made by a certain user
     */
    public List<Reservation> getReservationsByTelNumber(String telephoneNumber, boolean onlyFutureReservations) {
        String hql = (onlyFutureReservations ?
                "FROM " + clazz.getName() + " R WHERE R.user.telephoneNumber = :telephoneNumber" +
                        " AND R.reservationFrom > CURRENT_TIMESTAMP ORDER BY R.reservationCreationTimestamp DESC" :
                "FROM " + clazz.getName() + " R WHERE R.user.telephoneNumber = :telephoneNumber" +
                        " ORDER BY R.reservationCreationTimestamp DESC");
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Reservation> result = session.createQuery(hql, clazz)
                .setParameter("telephoneNumber", telephoneNumber)
                .getResultList();
        session.close();
        return result;
    }

    /**
     * Method retrieving time collisions between reservations and interval provided on a certain court
     *
     * @param courtId id of court which collisions are to be retrieved
     * @param fromDate start of an interval checked against present reservations
     * @param toDate end of an interval checked against present reservations
     * @return list of reservations clashing with interval provided
     */
    @Override
    public List<Reservation> getReservationCollisions(Long courtId, LocalDateTime fromDate, LocalDateTime toDate) {
        String hql = "FROM "+ clazz.getName() +" R WHERE" +
                " ((reservationFrom BETWEEN :start AND :end) OR" +
                " (reservationTo BETWEEN :start AND :end)) AND" +
                " court.id = :courtId";

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Reservation> result = session.createQuery(hql, clazz)
                .setParameter("start", fromDate)
                .setParameter("end", toDate)
                .setParameter("courtId", courtId)
                .getResultList();
        session.close();
        return result;
    }

}
