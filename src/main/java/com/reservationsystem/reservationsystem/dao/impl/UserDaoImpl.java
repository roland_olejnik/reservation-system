package com.reservationsystem.reservationsystem.dao.impl;

import com.reservationsystem.reservationsystem.dao.UserDao;
import com.reservationsystem.reservationsystem.domain.User;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

/**
 * DAO class of User
 *
 * @author Roland Olejník
 */
@Component
public class UserDaoImpl extends AbstractGenericDaoImpl<User, String> implements UserDao {

    public UserDaoImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

}
