package com.reservationsystem.reservationsystem.dao.impl;

import com.reservationsystem.reservationsystem.dao.CourtDao;
import com.reservationsystem.reservationsystem.domain.Court;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

/**
 * DAO class of Court
 *
 * @author Roland Olejník
 */
@Component
public class CourtDaoImpl extends AbstractGenericDaoImpl<Court, Long> implements CourtDao {
    public CourtDaoImpl(SessionFactory sessionFactory) {
        super(Court.class, sessionFactory);
    }
}
