package com.reservationsystem.reservationsystem.dao.impl;

import com.reservationsystem.reservationsystem.dao.GenericDao;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.google.common.base.Preconditions;

import java.util.List;

/**
 * Abstract class representing common functionality of DAOs
 *
 * Generic type T represents Entity type
 * Generic type S represents type of id type that Entity of generic type T uses
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
public abstract class AbstractGenericDaoImpl<T, S> implements GenericDao<T, S> {
    protected Class<T> clazz;

    protected SessionFactory sessionFactory;

    /**
     * Creates entity in its corresponding database table
     *
     * @param entity to be added into the database
     * @return created entity
     */
    @Override
    public T create(final T entity) {
        Preconditions.checkNotNull(entity);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.persist(entity);
        session.getTransaction().commit();
        session.close();
        return entity;
    }

    /**
     * Merges provided entity with the one already present in the database table, therefore updating it
     *
     * @param entity to be merged
     * @return updated entity
     */
    public T update(final T entity) {
        Preconditions.checkNotNull(entity);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        T result = session.merge(entity);
        session.getTransaction().commit();
        session.close();
        return result;
    }

    /**
     * Retrieves one entity from the database table depending on the id provided
     *
     * @param entityId id of the provided entity
     * @return retrieved entity or null if entity with the provided id does not exist
     */
    public T findOne(final S entityId) {
        Preconditions.checkNotNull(entityId);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        T result = session.get(clazz, entityId);
        session.close();
        return result;
    }

    /**
     * Returns list of all entities in its corresponding table
     *
     * @return list of all entities in its corresponding table
     */
    public List<T> findAll() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<T> result = session.createQuery("from " + clazz.getName(), clazz).getResultList();
        session.close();
        return result;
    }


    /**
     * Deletes provided entity from its corresponding table
     *
     * @param entity to be deleted
     */
    public void delete(final T entity) {
        Preconditions.checkNotNull(entity);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.remove(entity);
        session.getTransaction().commit();
        session.close();
    }


    /**
     * Deletes entity with provided id from its corresponding table
     *
     * @param entityId ide of entity to be deleted
     */
    public void deleteById(final S entityId) {
        Preconditions.checkNotNull(entityId);
        final T entity = findOne(entityId);
        Preconditions.checkNotNull(entity);
        delete(entity);
    }

}
