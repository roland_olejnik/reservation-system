package com.reservationsystem.reservationsystem.dao.impl;

import com.reservationsystem.reservationsystem.dao.CourtTypeDao;
import com.reservationsystem.reservationsystem.domain.CourtType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

/**
 * DAO class of CourtType
 *
 * @author Roland Olejník
 */
@Component
public class CourtTypeDaoImpl extends AbstractGenericDaoImpl<CourtType, Long> implements CourtTypeDao {
    public CourtTypeDaoImpl(SessionFactory sessionFactory) {
        super(CourtType.class, sessionFactory);
    }
}
