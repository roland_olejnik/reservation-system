package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.domain.CourtType;

/**
 * DAO interface of CourtType
 *
 * @author Roland Olejník
 */
public interface CourtTypeDao extends GenericDao<CourtType, Long> {
}
