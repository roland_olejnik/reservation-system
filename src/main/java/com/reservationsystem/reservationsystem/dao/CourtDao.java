package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.domain.Court;

/**
 * DAO interface of Court
 *
 * @author Roland Olejník
 */
public interface CourtDao extends GenericDao<Court, Long> {

}
