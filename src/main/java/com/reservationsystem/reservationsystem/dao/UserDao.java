package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.domain.User;

/**
 * DAO interface of User
 *
 * @author Roland Olejník
 */
public interface UserDao extends GenericDao<User, String> {
}
