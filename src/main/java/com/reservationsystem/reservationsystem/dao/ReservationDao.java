package com.reservationsystem.reservationsystem.dao;

import com.reservationsystem.reservationsystem.domain.Reservation;

import java.time.LocalDateTime;
import java.util.List;


/**
 * DAO interface of Reservation
 *
 * @author Roland Olejník
 */
public interface ReservationDao extends GenericDao<Reservation, Long> {

    List<Reservation> getReservationsByCourtId(long courtId);

    List<Reservation> getReservationsByTelNumber(String telephoneNumber, boolean onlyFutureReservations);

    List<Reservation> getReservationCollisions(Long courId, LocalDateTime fromDate, LocalDateTime toDate);
}
