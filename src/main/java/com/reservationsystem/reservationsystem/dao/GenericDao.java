package com.reservationsystem.reservationsystem.dao;

import java.util.List;

/**
 * Generic interface declaring common functionality in DAOs
 *
 * Generic type T represents Entity type
 * Generic type S represents type of id type that Entity of generic type T uses
 *
 * @author Roland Olejník
 */
public interface GenericDao<T, S> {

    T create(final T entity);

    T update(final T entity);

    T findOne(final S id);

    List<T> findAll();

    void delete(final T entity);

    void deleteById(final S entityId);

}
