package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;

import java.util.List;
import java.util.Optional;

/**
 * Generic interface for service classes declaring common functionality
 *
 * @author Roland Olejník
 */
public interface CrdService<T ,S> {
    T create(T user) throws CallInContextNotValidException;

    List<T> findAll();

    Optional<T> findOne(S telephoneNumber);

    void delete(S telephoneNumber);
}
