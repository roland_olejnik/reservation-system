package com.reservationsystem.reservationsystem.services.impl;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.dao.UserDao;
import com.reservationsystem.reservationsystem.domain.User;
import com.reservationsystem.reservationsystem.services.UserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for User entity
 *
 * @author Roland Olejník
 */
@Service
public class UserServiceImpl extends AbstractCrdServiceImpl<User, String, UserDao> implements UserService {

    public UserServiceImpl(UserDao dao) {
        super(dao);
    }

    /**
     * @param telephoneNumber of User to be updated
     * @param user entity with attributes to be changed in non-null values
     * @return updated User entity
     * @throws CallInContextNotValidException if User entity with provided telephoneNumber
     * does not exist in its database table
     */
    @Override
    public User update(String telephoneNumber, User user) throws CallInContextNotValidException {
        user.setTelephoneNumber(telephoneNumber);
        User retrievedUser = dao.findOne(telephoneNumber);
        if (Optional.ofNullable(retrievedUser).isEmpty()) throw new CallInContextNotValidException("User does not exist");
        if (Optional.ofNullable(user.getName()).isPresent()) retrievedUser.setName(user.getName());
        if (Optional.ofNullable(user.getSurname()).isPresent()) retrievedUser.setSurname(user.getSurname());
        return dao.update(retrievedUser);
    }

}
