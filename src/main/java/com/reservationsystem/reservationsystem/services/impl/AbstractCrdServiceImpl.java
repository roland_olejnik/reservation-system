package com.reservationsystem.reservationsystem.services.impl;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.dao.GenericDao;
import com.reservationsystem.reservationsystem.services.CrdService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Common create, retrieve and delete functionality used in interfaces
 *
 * Generic type T represents Entity type
 * Generic type S represents type of id type that Entity of generic type T uses
 * Generic type R represents type of DAO used by the Entity
 *
 * @author Roland Olejník
 */
@AllArgsConstructor
@Service
public abstract class AbstractCrdServiceImpl<T, S, R extends GenericDao<T, S>> implements CrdService<T, S> {
    protected R dao;

    @Override
    public T create(T entity) throws CallInContextNotValidException {
        return dao.create(entity);
    }

    @Override
    public List<T> findAll() {
        return dao.findAll();
    }

    @Override
    public Optional<T> findOne(S id) {
        return Optional.ofNullable(dao.findOne(id));
    }

    @Override
    public void delete(S id) {
        dao.deleteById(id);
    }
}
