package com.reservationsystem.reservationsystem.services.impl;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.dao.CourtTypeDao;
import com.reservationsystem.reservationsystem.dao.ReservationDao;
import com.reservationsystem.reservationsystem.dao.UserDao;
import com.reservationsystem.reservationsystem.domain.Reservation;
import com.reservationsystem.reservationsystem.services.ReservationService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service class for Reservation entity
 *
 * @author Roland Olejník
 */
@Service
public class ReservationServiceImpl extends AbstractCrdServiceImpl<Reservation, Long, ReservationDao> implements ReservationService {

    static final double DOUBLES_MULTIPLIER = 1.5;

    private final UserDao userDao;

    private final CourtTypeDao courtTypeDao;

    public ReservationServiceImpl(ReservationDao dao, UserDao userDao, CourtTypeDao courtTypeDao) {
        super(dao);
        this.userDao = userDao;
        this.courtTypeDao = courtTypeDao;
    }

    /**
     * @param reservation entity to be created
     * @return created Reservation entity
     * @throws CallInContextNotValidException if reservation cannot be created due to logical constraints
     */
    @Override
    public Reservation create(Reservation reservation) throws CallInContextNotValidException {
        if (Optional.ofNullable(userDao.findOne(reservation.getUser().getTelephoneNumber())).isEmpty()) //checks if user with telephone number exists
            userDao.create(reservation.getUser()); //if not then creates a new user
        if (reservation.getReservationFrom().isAfter(reservation.getReservationTo())){ //checks if reservation start precedes end
            throw new CallInContextNotValidException("Reservation end cannot precede reservation start");
        }
        reservation.setReservationCreationTimestamp(LocalDateTime.now()); // sets timestamp of the reservation
        if (!dao.getReservationCollisions(reservation.getCourt().getId(), reservation.getReservationFrom(),
                reservation.getReservationTo()).isEmpty()) { // checks if there is a time collision with other reservations
            throw new CallInContextNotValidException("Reservation is not possible due to time collision");
        }
        double priceOfCourtType = courtTypeDao.findOne(reservation.getCourt().getCourtType().getId()).getPrice();

        if (reservation.getDoubles()) reservation.setPrice(priceOfCourtType * DOUBLES_MULTIPLIER);
        else reservation.setPrice(priceOfCourtType);

        return dao.create(reservation);
    }

    /**
     * @param id of reservation to be updated
     * @param reservation entity with attributes to be updated in non-null values
     * @return updated Reservation entity
     * @throws CallInContextNotValidException if reservation cannot be updated due to logical constraints
     */
    @Override
    public Reservation update(Long id, Reservation reservation) throws CallInContextNotValidException {
        reservation.setId(id); //sets id of Reservation entity
        Reservation retrievedReservation = dao.findOne(id); //retrieves Reservation entity from the database

        LocalDateTime startToCompare = Optional.ofNullable(reservation.getReservationFrom()).isPresent() ?
                reservation.getReservationFrom() : retrievedReservation.getReservationFrom();
        LocalDateTime endToCompare = Optional.ofNullable(reservation.getReservationTo()).isPresent() ?
                reservation.getReservationTo() : retrievedReservation.getReservationTo();

        if (startToCompare.isAfter(endToCompare)){ //checks if updated reservations start will precede its end
            throw new CallInContextNotValidException("Reservation end cannot precede reservation start");
        }
        if (dao.getReservationCollisions(reservation.getCourt().getId(), startToCompare,
                endToCompare).size() > 1) { //checks if there is a time collision in a possibly updated entity
            throw new CallInContextNotValidException("Change of reservation is not possible due to time collision");
        }

        if (Optional.ofNullable(retrievedReservation).isEmpty()) //checks if reservation with provided id exists in table
            throw new CallInContextNotValidException("Reservation does not exist");
        if (Optional.ofNullable(reservation.getCourt()).isPresent())
            retrievedReservation.setCourt(reservation.getCourt());
        if (Optional.ofNullable(reservation.getReservationFrom()).isPresent())
            retrievedReservation.setReservationFrom(reservation.getReservationFrom());
        if (Optional.ofNullable(reservation.getReservationTo()).isPresent())
            retrievedReservation.setReservationTo(reservation.getReservationTo());
        return dao.update(retrievedReservation);
    }

    @Override
    public List<Reservation> findReservationsByCourt(Long courtId) {
        return dao.getReservationsByCourtId(courtId);
    }

    @Override
    public List<Reservation> findReservationsByUser(String telephoneNumber, Boolean onlyFuture) {
        return dao.getReservationsByTelNumber(telephoneNumber, onlyFuture);
    }

}
