package com.reservationsystem.reservationsystem.services.impl;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.dao.CourtDao;
import com.reservationsystem.reservationsystem.domain.Court;
import com.reservationsystem.reservationsystem.services.CourtService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for Court entity
 *
 * @author Roland Olejník
 */
@Service
public class CourtServiceImpl extends AbstractCrdServiceImpl<Court, Long, CourtDao> implements CourtService {

    public CourtServiceImpl(CourtDao dao) {
        super(dao);
    }


    /**
     * @param id of Court to be updated
     * @param court entity with attributes to be changed in non-null values
     * @return updated Court entity
     * @throws CallInContextNotValidException if Court entity with provided id does not exist in its database table
     */
    @Override
    public Court update(Long id, Court court) throws CallInContextNotValidException {
        court.setId(id);
        Court retrievedCourt = dao.findOne(id);
        if (Optional.ofNullable(retrievedCourt).isEmpty()) throw new CallInContextNotValidException("Court does not exist");
        if (Optional.ofNullable(court.getCourtType()).isPresent()) retrievedCourt.setCourtType(court.getCourtType());
        return dao.update(retrievedCourt);
    }
}
