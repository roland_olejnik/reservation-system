package com.reservationsystem.reservationsystem.services.impl;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.dao.CourtTypeDao;
import com.reservationsystem.reservationsystem.domain.CourtType;
import com.reservationsystem.reservationsystem.services.CourtTypeService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for CourtType entity
 *
 * @author Roland Olejník
 */
@Service
public class CourtTypeServiceImpl extends AbstractCrdServiceImpl<CourtType, Long, CourtTypeDao> implements CourtTypeService {

    public CourtTypeServiceImpl(CourtTypeDao dao) {
        super(dao);
    }

    /**
     * @param id of CourtType to be updated
     * @param courtType entity with attributes to be changed in non-null values
     * @return updated CourtType entity
     * @throws CallInContextNotValidException if CourtType entity with provided id does not exist in its database table
     */
    @Override
    public CourtType update(Long id, CourtType courtType) throws CallInContextNotValidException {
        courtType.setId(id);
        CourtType retrievedCourtType = dao.findOne(id);
        if (Optional.ofNullable(retrievedCourtType).isEmpty()) throw new CallInContextNotValidException("CourtType does not exist");
        if (Optional.ofNullable(courtType.getName()).isPresent()) retrievedCourtType.setName(courtType.getName());
        if (Optional.ofNullable(courtType.getPrice()).isPresent()) retrievedCourtType.setPrice(courtType.getPrice());
        return dao.update(retrievedCourtType);
    }

}