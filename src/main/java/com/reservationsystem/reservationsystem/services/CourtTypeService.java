package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.CourtType;

/**
 * Service interface for CourtType entity
 *
 * @author Roland Olejník
 */
public interface CourtTypeService extends CrdService<CourtType, Long> {
    CourtType update(Long id, CourtType courtType) throws CallInContextNotValidException;
}
