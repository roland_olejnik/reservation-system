package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.Court;

/**
 * Service interface for Court entity
 *
 * @author Roland Olejník
 */
public interface CourtService extends CrdService<Court, Long> {
    Court update(Long id, Court court) throws CallInContextNotValidException;
}
