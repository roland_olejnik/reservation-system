package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.Reservation;

import java.util.List;

/**
 * Service interface for Reservation entity
 *
 * @author Roland Olejník
 */
public interface ReservationService extends CrdService<Reservation, Long> {
    Reservation update(Long id, Reservation reservation) throws CallInContextNotValidException;

    List<Reservation> findReservationsByCourt(Long court_id);

    List<Reservation> findReservationsByUser(String telephoneNumber, Boolean onlyFuture);
}
