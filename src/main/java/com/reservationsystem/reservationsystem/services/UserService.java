package com.reservationsystem.reservationsystem.services;

import com.reservationsystem.reservationsystem.exceptions.CallInContextNotValidException;
import com.reservationsystem.reservationsystem.domain.User;

/**
 * Service interface for User entity
 *
 * @author Roland Olejník
 */
public interface UserService extends CrdService<User, String> {
    User update(String telephoneNumber, User user) throws CallInContextNotValidException;
}
