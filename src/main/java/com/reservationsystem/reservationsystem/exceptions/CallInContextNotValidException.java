package com.reservationsystem.reservationsystem.exceptions;

/**
 * Exception representing logical error in the context of application (e.g. start precedes end)
 *
 * @author Roland Olejník
 */
public class CallInContextNotValidException extends Exception {
    public CallInContextNotValidException(String errorMessage) {
        super(errorMessage);
    }
}
